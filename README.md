# Express Yourself

Website for publishing your own articles and tracking statistics.

Live version: [LIVE](https://express-yourself-app.herokuapp.com "LIVE")

## Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info

The application is used to read articles written by the creators. Each logged-in person can add their own articles and track their statistics. We can also add articles and authors to favorites to follow them.

|  Main page | Article page |
| --------  | --------  |
|  ![Main page](https://i.ibb.co/jVLhbqq/main-page.png "Main page") |  ![Article page](https://i.ibb.co/MDXyZ55/article-page.gif "Article page") |
|  **Category page** | **Write article**  |
|  ![Category page](https://i.ibb.co/PcsbcRj/category-page.png "Category page") |  ![Write article](https://i.ibb.co/RSgrC3c/write-article-page.png "Write article") |
|  **Register** | **Login**  |
|  ![Register](https://i.ibb.co/34v652N/register-desktop.png "Register") |  ![Login](https://i.ibb.co/YXBBRFm/login-desktop.png "Login") |
|  **Statistics** | **My favourites**  |
|  ![Statistics](https://i.ibb.co/ZTd0wVZ/stats-page.png "Statistics") |  ![My favourites](https://i.ibb.co/BqMt44t/my-favourite-page.png "My favourites") |

## Technologies
- Node.js 12.2.0 (server API)
- mongoDB
- React 16.7.0
- Redux 4.0.1

## Setup
To run this project, download [API](https://bitbucket.org/potaczeklukasz/express-yourself-backend/src/master/ "API") and run it:

```
$ npm install
$ node server
```

To run frontend:
```
$ npm install
$ npm start
```