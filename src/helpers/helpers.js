import getAge from 'get-age';

export const updateObject = (oldObject, updatedProperties) => {
	return {
		...oldObject,
		...updatedProperties
	};
};

export const checkValidity = (value, rules) => {
	let isValid = { valid: true };
	if (!rules) {
		return true;
	}

	if (rules.required && isValid.valid) {
		isValid = value.trim() !== '' && isValid.valid ? { valid: true } : { valid: false, error: "Field cannot be empty" };
	}

	if (rules.minLength && isValid.valid) {
		isValid = value.length >= rules.minLength && isValid.valid ? { valid: true } : { valid: false, error: `Field cannot be shorter then ${rules.minLength} characters` };
	}

	if (rules.maxLength && isValid.valid) {
		isValid = value.length <= rules.maxLength && isValid.valid ? { valid: true } : { valid: false, error: `Field cannot be longer then ${rules.maxLength} characters` };
	}

	if (rules.isEmail && isValid.valid) {
		const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
		isValid = pattern.test(value) && isValid.valid ? { valid: true } : { valid: false, error: `The entered text must be e-mail` };
	}

	return isValid;
}

export const ageRange = (birthday) => {
	const age = getAge(birthday);

	if (age <= 18) {
		return "<18";
	} else if (age <= 20) {
		return "18-20";
	} else if (age <= 26) {
		return "21-26";
	} else if (age <= 35) {
		return "27-35";
	} else if (age <= 45) {
		return "36-45";
	} else if (age <= 60) {
		return "46-60";
	} else {
		return "60>";
	}
}

const getDateArray = (start, end) => {
	var arr = [];
	var dt = new Date(start);
	while (dt <= end) {
		arr.push(new Date(dt).toISOString().slice(0, 10));
		dt.setDate(dt.getDate() + 1);
	}
	return arr;
}

export const setViewsData = (statistics) => {
	const datesArray = []; // array for all dates

	// push all dates from statistics to datesArray
	statistics.forEach(stats => {
		datesArray.push(new Date(stats.readDate).toISOString().slice(0, 10));
	});

	// sort datesArray from latest date to earliest date
	datesArray.sort((a, b) => {
		return new Date(a) - new Date(b);
	});

	// find first and last date
	const firstDate = new Date(datesArray[0]);
	const lastDate = new Date(datesArray[datesArray.length - 1]);

	// set array with all dates between first and last dates
	const filledDatesArray = getDateArray(firstDate, lastDate);

	// count number of visit for each date
	const counts = {};
	datesArray.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });

	// array for values
	const valueArray = [];

	// fill values array with number of visit to each day
	filledDatesArray.forEach((date, i) => {
		valueArray[i] = counts[date] || 0;
	});

	return { labels: filledDatesArray, values: valueArray };
}