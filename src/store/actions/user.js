import * as actionTypes from './actionTypes';

export const authUser = (token, user) => {
	return {
		type: actionTypes.AUTH_USER,
		token: token,
		user: user
	}
}

export const logoutUser = () => {
	return {
		type: actionTypes.LOGOUT_USER
	}
}

export const updateFavouriteWriters = (array) => {
	return {
		type: actionTypes.UPDATE_FAVOURITE_WRITERS,
		array
	}
}

export const updateFavouriteArticles = (array) => {
	return {
		type: actionTypes.UPDATE_FAVOURITE_ARTICLES,
		array
	}
}

export const updateUser = (userData) => {
	return {
		type: actionTypes.UPDATE_USER,
		userData
	}
}