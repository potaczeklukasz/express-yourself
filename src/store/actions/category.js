import * as actionTypes from './actionTypes';

export const fetchCategory = (categories) => {
	return {
		type: actionTypes.FETCH_CATEGORY,
		categories: categories
	}
}