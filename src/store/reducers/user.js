import * as actionTypes from '../actions/actionTypes';

const initialState = {
	user: null,
	token: null
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.AUTH_USER: {
			return {
				...state,
				token: action.token,
				user: action.user
			}
		}
		case actionTypes.LOGOUT_USER: {
			return {
				...state,
				user: null,
				token: null
			}
		}
		case actionTypes.UPDATE_FAVOURITE_WRITERS: {
			const user = {...state.user};
			user.favouriteWriters = action.array;

			return {
				...state,
				user: user
			}
		}
		case actionTypes.UPDATE_FAVOURITE_ARTICLES: {
			const user = {...state.user};
			user.favouriteArticles = action.array;
			
			return {
				...state,
				user: user
			}
		}
		case actionTypes.UPDATE_USER: {
			const updatedUser = {...state.user, ...action.userData};

			return {
				...state,
				user: updatedUser
			}
		}
		default:
			return state;
	}
}

export default reducer;