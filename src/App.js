import React, { Component } from 'react';
import Cookies from 'js-cookie';
import axios from 'axios';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actionCreators from './store/actions/index';
import { ageRange } from './helpers/helpers';

import Auth from './containers/Auth/Auth';
import Home from './containers/Home/Home';
import UserArticles from './containers/UserArticles/UserArticles';
import Navbar from './containers/Navbar/Navbar';
import Article from './containers/Article/Article';
import CategoryArticle from './containers/CategoryArticles/CategoryArticles';
import UserFavourites from './containers/UserFavourites/UserFavourites';
import UserProfile from './containers/UserProfile/UserProfile';
import UserSettings from './containers/UserSettings/UserSettings';
import SearchArticles from './containers/SearchArticles/SearchArticles';
import StatisticsModal from './components/StatisticsModal/StatisticsModal';

import styles from './App.module.scss';

class App extends Component {
	state = {
		showStatisticsModal: false
	}

	componentDidMount = () => {
		this.checkToken();
		this.getCategories();
	}

	checkStatisticsCookies = () => {
		setTimeout(() => {
			const location = this.props.location.pathname;

			if (!this.props.isAuthenticated && !Cookies.get('statistics') && location !== '/login' && location !== '/register') {
				this.setState({ showStatisticsModal: true });
			}
		}, 1000);

		if (this.props.isAuthenticated) {
			Cookies.set("gender", this.props.user.sex, { expires: 365 });
			Cookies.set("age", ageRange(this.props.user.birthDate), { expires: 365 });
			Cookies.set("statistics", true, { expires: 365 });
		}
	}

	setCookie = (e) => {
		if (e.target.value !== "Select...") {
			Cookies.set(e.target.name, e.target.value, { expires: 365 });

			if (Cookies.get("gender") && Cookies.get("age")) {
				this.setState({ showStatisticsModal: false });
				Cookies.set("statistics", true, { expires: 365 });
			}
		}
	}

	checkToken = async () => {
		const token = Cookies.get('token');
		if (token) {
			const res = await axios.get('/api/users', {
				headers: { 'Authorization': `Bearer ${token}` }
			});
			this.props.authUser(token, res.data.user);
		}
	}

	getCategories = async () => {
		const res = await axios.get('/api/category');
		this.props.fetchCategory(res.data.categories);
	}

	render() {
		this.checkStatisticsCookies();

		const location = this.props.location.pathname;
		let routes = (
			<Switch>
				<Route path="/(login|register)" component={Auth} />
				<Route path="/profile/articles/:articleId/statistics" component={UserArticles} />
				<Route path="/profile/articles" component={UserArticles} />
				<Route path="/profile/favourites" component={UserFavourites} />
				<Route path="/profile/settings" component={UserSettings} />
				<Route path="/user/:userId" component={UserProfile} />
				<Route path="/article/:articleId" component={Article} />
				<Route path="/category/:category" component={CategoryArticle} />
				<Route path="/search" component={SearchArticles} />
				<Route path="/" exact component={Home} />
				<Redirect to="/" />
			</Switch>
		)

		return (
			<div className={location !== '/login' && location !== '/register' ? styles.app : null}>
				{location !== '/login' && location !== '/register' ? <Navbar /> : null}
				{routes}
				{this.state.showStatisticsModal ? <StatisticsModal clickHandler={this.setCookie} /> : null}
				<link
					rel="stylesheet"
					href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
					integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
					crossOrigin="anonymous"
				/>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token, user) => dispatch(actionCreators.authUser(token, user)),
		fetchCategory: (categories) => dispatch(actionCreators.fetchCategory(categories))
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));