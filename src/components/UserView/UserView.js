import React from 'react';
import { withRouter } from 'react-router-dom';

import styles from './UserView.module.scss';

import Button from '../UI/Button/Button';

const UserView = (props) => {
	return (
		<div className={`${props.className} ${styles.userProfile}`}>
			<img src={`/api/image/${props.user.imagePath}`} alt="user" />
			<p>{props.user.firstName}</p>
			<p>{props.user.lastName}</p>
			<Button className={styles.button} clickHandler={() => props.history.push(`/user/${props.user._id}`)} type="blue">VIEW PROFILE</Button>
		</div>
	)
}

export default withRouter(UserView);
