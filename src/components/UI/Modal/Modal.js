import React from 'react';

import Button from '../Button/Button';

import styles from './Modal.module.scss';

const Modal = (props) => {
	return (
		<div className={styles.background} onClick={props.confirmHandler}>
			<div className={styles.modal}>
				<p className={styles.text}>{props.text}</p>
				<Button className={styles.button} clickHandler={props.confirmHandler} type="green"> OK </Button>
			</div>
		</div>
	)
}

export default Modal;