import React from 'react';
import { HashLoader } from 'react-spinners';

import styles from './Spinner.module.scss';

const Spinner = (props) => {
	return (
		<div className={styles.spinner}>
			<HashLoader
				sizeUnit={"px"}
				size={props.size}
				color={props.color || "#8CC64D"}
			/>
		</div>
	)
}

export default Spinner;
