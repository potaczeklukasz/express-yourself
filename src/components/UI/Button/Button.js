import React from 'react';
import PropTypes from 'prop-types';

import styles from './Button.module.scss';

const button = (props) => {
	return (
		<div
			disabled={props.disabled}
			className={`${styles.button} ${styles[props.type]} ${props.className}`}
			onClick={props.clickHandler}>{props.children}</div>
	);
}

export default button;

button.propTypes = {
	disabled: PropTypes.bool,
	type: PropTypes.string.isRequired,
	clickHandler: PropTypes.func.isRequired
}