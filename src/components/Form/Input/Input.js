import React from 'react';

import styles from './Input.module.scss';

const input = (props) => {
	let inputElement = null;
	let inputClasses = styles.inputElement;

	if (!props.valid.valid && props.shouldValidate && props.touched) {
		inputClasses = `${styles.invalid} ${styles.inputElement}`;
	}

	switch (props.elementType) {
		case 'input':
			inputElement = <input
				className={inputClasses}
				{...props.elementConfig}
				value={props.value}
				onFocus={props.onChange}
				onChange={props.onChange} />
			break;
		case 'select':
			inputElement = <select
				className={`${inputClasses} ${styles.select}`}
				{...props.elementConfig}
				value={props.value}
				onFocus={props.onChange}
				onChange={props.onChange}>
				{props.selectOptions.map((option, key) => <option key={key} value={option.value}>{option.display}</option>)}
			</select>
			break;
		default:
			inputElement = <input
				className={inputClasses}
				{...props.elementConfig}
				value={props.value}
				onFocus={props.onChange}
				onChange={props.onChange} />
	}

	return (
		<div className={props.elementConfig.hidden ? styles.inputHidden : styles.input}>
			<label className={styles.label}>{props.label}</label>
			{inputElement}
			<p className={styles.error}>{(!props.valid.valid && props.showError) ? props.valid.error : null}</p>
		</div>
	);
}

export default input;