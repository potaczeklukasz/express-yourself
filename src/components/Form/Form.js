import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './Form.module.scss';

import Input from './Input/Input';

export default class Form extends Component {
	render() {
		const formElementsArray = [];
		for (const key in this.props.fields) {
			formElementsArray.push({
				key: key,
				config: this.props.fields[key]
			});
		}

		const form = formElementsArray.map(formElement => (
			<Input
				key={formElement.key}
				elementType={formElement.config.elementType}
				elementConfig={formElement.config.elementConfig}
				value={formElement.config.value}
				label={formElement.config.label}
				valid={formElement.config.valid}
				showError={this.props.showInputsErrors}
				shouldValidate={formElement.config.validation}
				touched={formElement.config.touched}
				selectOptions={formElement.config.options}
				onChange={(event) => this.props.inputChangedHandler(event, formElement.key)} />
		));

		return (
			<form className={styles.form} onSubmit={this.props.submitHandler}>
				{form}
				<p className={styles.error}>{this.props.error}</p>
				{this.props.children}
			</form>
		)
	}
}

Form.propTypes = {
	fields: PropTypes.object.isRequired,
	error: PropTypes.string,
	inputChangedHandler: PropTypes.func.isRequired,
	showInputsErrors: PropTypes.bool
}