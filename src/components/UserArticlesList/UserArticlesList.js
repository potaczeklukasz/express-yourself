import React, { Component } from 'react';

import ControlArticle from './ControlArticle/ControlArticle';

export default class UserArticlesList extends Component {
	render() {
		return (
			<>
				{this.props.articles.map((article, i) => <ControlArticle editArticle={this.props.editArticle} key={article._id} article={article} updateId={this.props.updateId}/>)}
			</>
		)
	}
}
