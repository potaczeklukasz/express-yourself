import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import styles from './ControlArticle.module.scss';

import Button from '../../UI/Button/Button';

class ControlArticle extends Component {
	updateId = () => {
		this.props.updateId(this.props.article._id);
		this.props.history.push(`/profile/articles/${this.props.article._id}/statistics`);
	}

	render() {
		return (
			<div
				className={styles.article}
				style={{ backgroundImage: `url(/api/image/${this.props.article.imagePath})` }}>

				<p className={styles.title}>{this.props.article.title}</p>

				<div className={styles.buttons}>
					<Button className={styles.button} clickHandler={this.updateId} type="green">STATS</Button>
					<Button className={styles.button} clickHandler={this.props.editArticle.bind(null, this.props.article._id)} type="blue">EDIT</Button>
				</div>
			</div >
		)
	}
}

export default withRouter(ControlArticle);