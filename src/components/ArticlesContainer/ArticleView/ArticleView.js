import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import styles from './ArticleView.module.scss';

export default class ArticleView extends Component {
	render() {
		return (
			<div
				className={`${this.props.className} ${styles.article}`}
				style={{ backgroundImage: `url(/api/image/${this.props.article.imagePath})` }}>

				<p className={styles.category}>{this.props.article.category}</p>
				<p className={styles.title}>{this.props.article.title}</p>

				<div className={styles.shadow}>
					<Link to={`/article/${this.props.article._id}`} className={styles.button}>READ ARTICLE </Link>
				</div>
			</div >
		)
	}
}
