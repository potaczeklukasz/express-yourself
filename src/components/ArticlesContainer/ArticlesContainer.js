import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import styles from './ArticlesContainer.module.scss';

import searchIcon from '../../images/search.png';

import ArticleView from './ArticleView/ArticleView';

class ArticlesContainer extends Component {
	divideArticles = () => {
		const leftArticles = [];
		const rightArticles = [];

		this.props.articles.forEach((article, i) => {
			if (i < 3) {
				leftArticles.push(<ArticleView className={styles.article} article={article} key={article._id} />);
			} else {
				rightArticles.push(<ArticleView className={styles.article} article={article} key={article._id} />);
			}
		});
		return { leftArticles, rightArticles }
	}

	render() {
		const articles = this.divideArticles();

		return (
			<div className={styles.articlesContainer}>
				<div className={styles.titleBox}>
					<div className={styles.titles}>
						<h3>{this.props.title}</h3>
						<p className={styles.title}>{this.props.subtitle}</p>
					</div>
					<img className={styles.searchIcon} onClick={() => this.props.history.push('/search')} src={searchIcon} alt='search' />
				</div>
				<div className={styles.articlesGroup}>
					<div className={styles.articlesLeft}>
						{articles.leftArticles}
					</div>
					<div className={styles.articlesRight}>
						{articles.rightArticles}
					</div>
				</div>
			</div>
		)
	}
}

export default withRouter(ArticlesContainer);