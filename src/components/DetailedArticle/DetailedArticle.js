import React from 'react';
import { withRouter } from 'react-router-dom';

import styles from './DetailedArticle.module.scss';

import Button from '../UI/Button/Button';

const DetailedArticle = (props) => {
	const clickHandler = () => {
		props.history.push(`/article/${props.article._id}`);
		if (props.onClick) props.onClick(null, props.article._id);
	}

	return (
		<div className={styles.detailedArticle}>
			<div className={styles.coverPhoto} style={{ backgroundImage: `url("/api/image/${props.article.imagePath}")` }}></div>
			<div className={styles.details}>
				<div>
					<p className={styles.title}>{props.article.title}</p>
					<p className={styles.subtitle}>{props.article.subtitle}</p>
				</div>
				<Button className={styles.button} clickHandler={clickHandler} type="blue">READ ARTICLE</Button>
			</div>
		</div>
	)
}

export default withRouter(DetailedArticle);
