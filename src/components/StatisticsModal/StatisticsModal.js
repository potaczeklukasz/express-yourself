import React, { Component } from 'react';

import styles from './StatisticsModal.module.scss';

export default class StatisticsModal extends Component {
	render() {
		return (
			<div className={styleMedia.statisticsModal}>
				<div className={styles.background}>

				</div>
				<div className={styles.form}>
					<p>Please enter your gender and age.</p>
					<label>Gender</label>
					<select name="gender" onChange={this.props.clickHandler}>
						<option>Select...</option>
						<option value="M">Men</option>
						<option value="W">Women</option>
						<option value="O">Other</option>
					</select>
					<label>Age</label>
					<select name="age" onChange={this.props.clickHandler}>
						<option>Select...</option>
						<option value="<18">&lt; 18</option>
						<option value="18-20">18-20</option>
						<option value="21-26">21-26</option>
						<option value="27-35">27-35</option>
						<option value="36-45">36-45</option>
						<option value="46-60">46-60</option>
						<option value="60>">60 &gt;</option>
					</select>
				</div>
			</div>
		)
	}
}
