import React from 'react';

import styles from './Comment.module.scss';

const Comment = (props) => {
	return (
		<div className={styles.comment}>
			<div className={styles.userInfo}>
				<img className={styles.userImage} src={`/api/image/${props.comment.userImage}`} alt="user" />
				<div className={styles.userData}>
					<p className={styles.userName}>{props.comment.userName}</p>
					<p className={styles.date}>{props.comment.dateCreated}</p>
				</div>
			</div>
			<div className={styles.text}>
				{props.comment.comment}
			</div>
		</div>
	)
}

export default Comment;
