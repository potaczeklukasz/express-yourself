import React, { Component } from 'react';
import axios from 'axios';

import styles from './UserProfile.module.scss';

import ArticleView from '../../components/ArticlesContainer/ArticleView/ArticleView';

export default class UserProfile extends Component {
	state = {
		user: null,
		articles: null
	}

	componentDidMount = async () => {
		const user = await axios.get(`/api/users/${this.props.match.params.userId}`);
		this.setState({ user: user.data.user });

		const articles = await axios.get(`/api/article/user/${this.props.match.params.userId}`);
		this.setState({ articles: articles.data.articles });
	}

	render() {
		return (
			<div className={styles.userProfile}>
				{this.state.user ? <>
					<p className={styles.name}>{this.state.user.firstName} {this.state.user.lastName}</p>
					<img src={`/api/image/${this.state.user.imagePath}`} alt="user" />
				</> : null}

				<h3>Articles</h3>
				{this.state.articlesMessage ? <p className={styles.message}>{this.state.articlesMessage}</p> : null}
				{this.state.articles ? <div className={styles.articles}>
					{this.state.articles.map((article, i) => <ArticleView className={styles.article} article={article} key={article._id} />)}
				</div> : null}
			</div>
		)
	}
}
