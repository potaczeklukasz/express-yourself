import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import settingsFields from './settingsFields';
import { checkValidity, updateObject } from '../../helpers/helpers';
import * as actionCreators from '../../store/actions/index';

import Form from '../../components/Form/Form';
import Button from '../../components/UI/Button/Button';

import styles from './UserSettings.module.scss';

class UserSettings extends Component {
	state = {
		fields: settingsFields,
		errorMessage: '',
		showInputsErrors: false
	}

	componentDidMount = () => {
		if (this.props.isAuthenticated) this.setData();
	}

	setData = () => {
		const fields = this.state.fields;

		fields.email.value = this.props.user.email;
		fields.firstName.value = this.props.user.firstName;
		fields.lastName.value = this.props.user.lastName;
		fields.sex.value = this.props.user.sex;
		fields.birthDate.value = new Date(this.props.user.birthDate).toISOString().slice(0, 10);
		fields.imagePath.value = this.props.user.imagePath;

		this.setState({ fields });
	}

	sendFile = async (image) => {
		if (image) {
			const formData = new FormData();
			formData.append('image', image, image.name);

			const res = await axios({
				url: '/api/image/upload',
				method: 'POST',
				headers: { 'Authorization': `Bearer ${this.props.token}`, 'Content-Type': 'multipart/form-data' },
				data: formData
			});
			if (!res.data.error) {
				const fields = this.state.fields;
				fields.imagePath.value = res.data.fileName;
				fields.imagePath.valid = { valid: true }
				this.setState({ fields });
			}
		}
	}

	inputChangedHandler = (event, fieldName) => {
		const updatedFields = updateObject(this.state.fields, {
			[fieldName]: updateObject(this.state.fields[fieldName], {
				value: event.target.value,
				valid: checkValidity(event.target.value, this.state.fields[fieldName].validation),
				touched: true
			})
		});
		this.setState({ fields: updatedFields });
	}

	editData = async () => {
		const user = {
			email: this.state.fields.email.value,
			firstName: this.state.fields.firstName.value,
			lastName: this.state.fields.lastName.value,
			sex: this.state.fields.sex.value,
			birthDate: new Date(this.state.fields.birthDate.value),
			imagePath: this.state.fields.imagePath.value
		}

		const res = await axios.patch(`/api/users/${this.props.user._id}`, user, { headers: { 'Authorization': `Bearer ${this.props.token}` } });
		if (res.data.error) alert(res.data.description);

		this.props.updateUser(user);
	}

	render() {
		return (
			<div className={styles.UserSettings}>
				{!this.props.isAuthenticated ? <Redirect to='/' /> : null}
				<h3>Settings</h3>
				{this.props.user ? <>
					<img src={`/api/image/${this.state.fields.imagePath.value}`} alt="user" />
					<label className={styles.label} htmlFor="upload-photo">Select photo...</label>
					<input id="upload-photo" type="file" onChange={(e) => this.sendFile(e.target.files[0])} />
					<Form
						fields={this.state.fields}
						inputChangedHandler={this.inputChangedHandler}
						error={this.state.errorMessage}
						showInputsErrors={this.state.showInputsErrors}>
						<Button className={styles.button} clickHandler={this.editData} type="green">EDIT DATA</Button>
					</Form>
				</> : null}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user,
		token: state.user.token
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateUser: (userData) => dispatch(actionCreators.updateUser(userData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSettings);