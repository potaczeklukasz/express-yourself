import React, { Component } from 'react';
import axios from 'axios';
import BottomScrollListener from 'react-bottom-scroll-listener';

import DetailedArticle from '../../components/DetailedArticle/DetailedArticle';
import Button from '../../components/UI/Button/Button';

import lazyLoadingGif from '../../images/loading.gif';

import styles from './SearchArticles.module.scss';

export default class SearchArticles extends Component {
	state = {
		page: 1,
		totalPages: null,
		articles: null,
		error: null,
		searchText: "",
		lazyLoading: false,
		loadingGif: false
	}

	checkIfBottom = () => {
		const d = document.documentElement;
		const offset = d.scrollTop + window.innerHeight;

		// if user is on the bottom of page, load more articles
		if (offset === document.body.scrollHeight && !this.state.lazyLoading) {
			this.setState({ lazyLoading: true });
			this.lazyLoad();
		}
	}

	getArticles = async () => {
		try {
			this.setState({ loadingGif: true, error: null, articles: null });
			const res = await axios.post(`/api/article/search/1`, { searchText: this.state.searchText });

			this.setState({ articles: res.data.articles, totalPages: res.data.totalPages, error: null, page: res.data.currentPage + 1, loadingGif: false });

			if (res.data.totalPages >= res.data.currentPage + 1) this.checkIfBottom();
			else this.setState({ loadingGif: false });
		} catch (err) {
			this.setState({ error: err.response.data.description, loadingGif: false, articles: null });
		}
	}

	changeHandler = (e) => {
		this.setState({ searchText: e.target.value });
	}

	lazyLoad = async () => {
		try {
			this.setState({ lazyLoading: true, loadingGif: true });
			const res = await axios.post(`/api/article/search/${this.state.page}`, { searchText: this.state.searchText });

			// add loaded articles to already displayed articles
			const articles = this.state.articles;
			res.data.articles.forEach(article => {
				articles.push(article);
			});

			this.setState({ articles: articles, totalPages: res.data.totalPages, spinner: false, error: null, page: res.data.currentPage + 1, lazyLoading: false });

			// if this was last page of articles - remove loading gif
			if (res.data.totalPages < res.data.currentPage + 1) this.setState({ loadingGif: false });
		} catch (err) {
			this.setState({ error: err.response.data.description, spinner: false, loadingGif: false });
		}
	}

	render() {
		return (
			<>
				<BottomScrollListener onBottom={() => this.state.totalPages >= this.state.page ? this.checkIfBottom() : null} />
				<div className={styles.controls}>
					<input placeholder="Search..." type="text" className={styles.input} onChange={this.changeHandler} />
					<Button className={styles.button} clickHandler={this.getArticles} type="green"> SEARCH </Button>
				</div>
				<div className={styles.searchArticles}>
					{this.state.error ? <p className={styles.errorMessage}>{this.state.error}</p> : null}
					{this.state.articles ? <>
						{this.state.articles.map((article, i) => <DetailedArticle key={article._id} article={article} />)}
					</> : null}
					{this.state.loadingGif && !this.state.spinner ? <img className={styles.loadingGif} src={lazyLoadingGif} alt='loading' /> : null}
				</div>
			</>
		)
	}
}