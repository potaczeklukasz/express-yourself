import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';

import styles from './UserFavourites.module.scss';

import ArticleView from '../../components/ArticlesContainer/ArticleView/ArticleView';
import UserView from '../../components/UserView/UserView';

export class UserFavourites extends Component {
	state = {
		favouriteAuthors: null,
		favouriteArticles: null,
		authorsMessage: "",
		articlesMessage: ""
	}
	componentDidMount = async () => {
		if (this.props.user) {

			try {
				const favouriteAuthors = await axios.post('/api/users/many', {
					idArray: this.props.user.favouriteWriters
				});
				this.setState({ favouriteAuthors: favouriteAuthors.data.users });
			} catch (err) {
				this.setState({ authorsMessage: err.response.data.description });
			}

			try {
				const favouriteArticles = await axios.post('/api/article/many', {
					idArray: this.props.user.favouriteArticles
				});
				this.setState({ favouriteArticles: favouriteArticles.data.articles });
			} catch (err) {
				this.setState({ articlesMessage: err.response.data.description });
			}
		}
	}

	render() {
		return (
			<div className={styles.userFavourites}>
				{!this.props.user ? <Redirect to="/" /> : null}
				<h2>My favourites</h2>

				<h3>Authors</h3>
				{this.state.authorsMessage ? <p className={styles.message}>{this.state.authorsMessage}</p> : null}
				{this.state.favouriteAuthors ? <div className={styles.favouriteAuthors}>
					{this.state.favouriteAuthors.map((user, i) => <UserView className={styles.user} user={user} key={user._id} />)}
				</div> : null}

				<h3>Articles</h3>
				{this.state.articlesMessage ? <p className={styles.message}>{this.state.articlesMessage}</p> : null}
				{this.state.favouriteArticles ? <div className={styles.favouriteArticles}>
					{this.state.favouriteArticles.map((article, i) => <ArticleView className={styles.article} article={article} key={article._id} />)}
				</div> : null}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user,
		token: state.user.token
	};
};

export default connect(mapStateToProps)(UserFavourites)
