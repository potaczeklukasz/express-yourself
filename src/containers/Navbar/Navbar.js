import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import { Dropdown } from 'react-bootstrap';
import { IoMdLogIn, IoMdLogOut } from "react-icons/io";
import Cookies from 'js-cookie';

import { mobileWidth } from '../../settings';

import styles from './Navbar.module.scss';

import logoImg from '../../images/logo.png';
import showMenuImg from '../../images/menu.png';
import hideMenuImg from '../../images/hide.png';

import Button from '../../components/UI/Button/Button';

class Navbar extends Component {
	state = {
		maxElementsToDisplay: (~~((window.innerWidth - 300) / 100)) - 1,
		deviceWidth: window.innerWidth,
		mobileMenu: "hide"
	}

	componentDidMount = () => {
		// recalculate number of categories to display outside "More"
		window.onresize = () => {
			const maxElements = (~~((window.innerWidth - 300) / 100)) - 1

			// if number is different from number of current displayed elements - rerender navbar
			if (maxElements !== this.state.maxElementsToDisplay) {
				this.setState({ maxElementsToDisplay: maxElements, deviceWidth: window.innerWidth });
			} else {
				this.setState({ deviceWidth: window.innerWidth });
			}
		}
	}

	divideCategories = () => {
		if (this.props.categories) {
			const maxElementsToDisplay = this.state.maxElementsToDisplay;
			const totalElements = this.props.categories.length;

			// check if all elements can be displayed outside "More"
			// "+ 2" is to prevent rendering dropdown with one element
			if (this.props.categories.length <= maxElementsToDisplay + 2 || this.state.deviceWidth <= mobileWidth) {
				const normalElements = [];

				// render all categories outside "More"
				for (let i = 0; i < this.props.categories.length; i++) {
					const category = this.props.categories[i].category;
					normalElements.push(<Link onClick={this.toggleMobileMenu} className={styles.category} to={`/category/${category}`} key={category}>{category}</Link>);
				}

				return { normalElements, hiddenElements: [], moreRequired: false }
			} else {
				const normalElements = [];
				const hiddenElements = [];

				// render elements to display outside "More"
				for (let i = 0; i <= maxElementsToDisplay; i++) {
					const category = this.props.categories[i].category;
					normalElements.push(<Link className={styles.category} to={`/category/${category}`} key={category}>{category}</Link>);
				}

				// render elements to display inside "More"
				for (let i = maxElementsToDisplay + 1; i < totalElements; i++) {
					const category = this.props.categories[i].category;
					hiddenElements.push(<Dropdown.Item key={category} onClick={() => this.props.history.push(`/category/${category}`)}>{category}</Dropdown.Item>);
				}

				return { normalElements, hiddenElements, moreRequired: true };
			}
		} else { // if there is no fetched categories, display nothing
			return { normalElements: [], hiddenElements: [], moreRequired: false };
		}
	}

	log = () => {
		if (this.props.isAuthenticated) {
			this.props.logoutUser();
			Cookies.remove("token");
		}
		this.props.history.push('/login');
	}

	toggleMobileMenu = () => this.state.mobileMenu === "show" ? this.setState({ mobileMenu: "hide" }) : this.setState({ mobileMenu: "show" });

	render() {
		const categories = this.divideCategories();

		const mobileMenu = (
			<div className={`${styles.navbar} ${styles[this.state.mobileMenu]}`}>

				<img className={styles.logo} src={logoImg} alt='logo' onClick={() => this.props.history.push('/')} />
				<img onClick={this.toggleMobileMenu} className={styles.menuImg} src={this.state.mobileMenu === "show" ? hideMenuImg : showMenuImg} alt='logo' />

				{this.props.isAuthenticated ?
					<>
						<img className={styles.user} src={`/api/image/${this.props.user.imagePath}`} alt='user' />
						<Link onClick={this.toggleMobileMenu} className={styles.link} to={`/profile/favourites`}>My favourites</Link>
						<Link onClick={this.toggleMobileMenu} className={styles.link} to={`/profile/articles/new`}>My articles</Link>
						<Link onClick={this.toggleMobileMenu} className={styles.link} to={`/profile/settings`}>Settings</Link>
					</> : null}

				{this.props.isAuthenticated ?
					<Button className={styles.button} clickHandler={this.log} type="red"> <IoMdLogOut size={24} /> Logout </Button> :
					<Button className={styles.button} clickHandler={this.log} type="green"> <IoMdLogIn size={24} /> Login </Button>}

				<div className={styles.navigation}>
					{categories.normalElements}
				</div>
			</div>
		);

		const normalMenu = (
			<div className={styles.navbar}>
				<img className={styles.logo} src={logoImg} alt='logo' onClick={() => this.props.history.push('/')} />
				<div className={styles.navigation}>
					{categories.normalElements}

					{/* If there are categories to display inside "More", render Dropdown */}
					{categories.moreRequired ?
						<Dropdown>
							<Dropdown.Toggle variant="success" id="dropdown-basic" className={styles.dropdownToggle}>More</Dropdown.Toggle>
							<Dropdown.Menu>
								{categories.hiddenElements}
							</Dropdown.Menu>
						</Dropdown> : null}
				</div>
				<div className={styles.userNavigation}>
					{this.props.isAuthenticated ?
						<Button className={styles.button} clickHandler={this.log} type="red"> <IoMdLogOut size={24} /> Logout </Button> :
						<Button className={styles.button} clickHandler={this.log} type="green"> <IoMdLogIn size={24} /> Login </Button>}

					{/* User options dropdown */}
					{this.props.isAuthenticated ?
						<Dropdown>
							<Dropdown.Toggle variant="success" id="dropdown-basic" className={styles.dropdownToggle}>
								<img className={styles.user} src={`/api/image/${this.props.user.imagePath}`} alt='user' />
							</Dropdown.Toggle>
							<Dropdown.Menu className={styles.userDropdownMenu}>
								<Dropdown.Item onClick={() => { this.props.history.push('/profile/favourites') }}>My favourites</Dropdown.Item>
								<Dropdown.Item onClick={() => { this.props.history.push('/profile/articles/new') }}>My articles</Dropdown.Item>
								<Dropdown.Item onClick={() => { this.props.history.push('/profile/settings') }}>Settings</Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown> : null}
				</div>
			</div>
		);

		return this.state.deviceWidth <= mobileWidth ? mobileMenu : normalMenu;
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		categories: state.category.categories,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		logoutUser: () => dispatch(actionCreators.logoutUser())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));