import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import * as actionCreators from '../../../store/actions/index';

import styles from './AuthorDetails.module.scss';

class AuthorDetails extends Component {
	state = {
		author: null
	}

	componentDidMount = async () => {
		const res = await axios.get(`/api/users/${this.props.authorId}`);
		if (res.data.user) this.setState({ author: res.data.user });
	}

	addAuthorToFavourite = async () => {
		if (this.props.user) {
			const favouriteWriters = [...this.props.user.favouriteWriters];
			if (favouriteWriters.indexOf(this.state.author._id) === -1) {
				favouriteWriters.push(this.state.author._id);
			} else {
				favouriteWriters.splice(favouriteWriters.indexOf(this.state.author._id), 1);
			}
			await axios.patch(`/api/users/${this.props.user._id}/favourite/writers`, {
				favouriteWriters: favouriteWriters
			}, { headers: { Authorization: `Bearer ${this.props.token}` } });
			this.props.updateFavouriteWriters(favouriteWriters);
		}
	}

	redirectToUser = () => {
		this.props.history.push(`/user/${this.state.author._id}`);
	}

	render() {
		return (
			<>
				{this.state.author ?
					<div className={styles.authorDetails}>
						<img className={styles.authorImage} src={`/api/image/${this.state.author.imagePath}`} alt='author' onClick={this.redirectToUser} />
						<div className={styles.authorName}>
							<p className={styles.header}>WRITTEN BY</p>
							<p className={styles.name} onClick={this.redirectToUser}>{this.state.author.firstName} {this.state.author.lastName}</p>
						</div>
						<img
							className={styles.favImage}
							src={this.props.user && this.props.user.favouriteWriters.indexOf(this.state.author._id) !== -1 ? require('../../../images/heart-full.png') : require('../../../images/heart.png')}
							alt='author'
							onClick={this.addAuthorToFavourite} />
					</div> : null}
			</>
		)
	}
}

const mapStateToProps = state => {
	return {
		token: state.user.token,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateFavouriteWriters: (array) => dispatch(actionCreators.updateFavouriteWriters(array))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AuthorDetails));