import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import styles from './Comments.module.scss';

import Comment from '../../../components/Comment/Comment';
import Button from '../../../components/UI/Button/Button';

class Comments extends Component {
	state = {
		commentText: null
	}

	addComment = async () => {
		if (this.state.commentText) {
			await axios.post(`/api/article/${this.props.articleId}/add/comment`, {
				userName: `${this.props.user.firstName} ${this.props.user.lastName}`,
				userId: this.props.user._id,
				comment: this.state.commentText,
				userImage: this.props.user.imagePath
			}, { headers: { Authorization: `Bearer ${this.props.token}` } });

			this.props.addComment({
				userName: `${this.props.user.firstName} ${this.props.user.lastName}`,
				userId: this.props.user._id,
				comment: this.state.commentText,
				userImage: this.props.user.imagePath,
				dateCreated: new Date().toISOString()
			});

			this.setState({ commentText: null });
		}
	}

	inputHandler = (e) => {
		this.setState({ commentText: e.target.value });
	}

	render() {
		return (
			<>
				{this.props.isAuthenticated ?
					<div className={styles.input}>
						<img src={`/api/image/${this.props.user.imagePath}`} alt="user" />
						<input type="text" placeholder="Add a public comment..." onInput={this.inputHandler} />
						<Button className={styles.button} clickHandler={this.addComment} type="green">ADD</Button>
					</div> : null}
				<div className={styles.comments}>
					<p className={styles.commentsNumber}>Number of comments: {this.props.comments.length}</p>
					{this.props.comments.map((comment, i) => <Comment key={comment._id} comment={comment} />)}
				</div>
			</>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user,
		token: state.user.token
	};
};

export default connect(mapStateToProps)(Comments);