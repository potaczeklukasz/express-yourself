import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Cookies from 'js-cookie';

import * as actionCreators from '../../store/actions/index';

import Spinner from '../../components/UI/Spinner/Spinner';
import AuthorDetails from './AuthorDetails/AuthorDetails';
import Comments from './Comments/Comments';
import DetailedArticle from '../../components/DetailedArticle/DetailedArticle';

import styles from './Article.module.scss';

class Article extends Component {
	state = {
		article: null,
		articles: null,
		spinner: true
	}

	componentDidMount = () => {
		this.loadArticle();
	}

	loadArticle = async (callback, id) => {
		window.scrollTo(0, 0);
		let articleId = this.props.match.params.articleId;
		if (id) articleId = id;

		this.setState({ spinner: true });
		const res = await axios.get(`/api/article/${articleId}`, { params: { gender: Cookies.get("gender"), age: Cookies.get("age") } });

		this.setState({ article: res.data.article, spinner: false });
		if (callback) callback();
		this.fetchProposedArticles();
	}

	addArticleToFavourite = async () => {
		if (this.props.user) {
			const favouriteArticles = [...this.props.user.favouriteArticles];
			if (favouriteArticles.indexOf(this.state.article._id) === -1) {
				favouriteArticles.push(this.state.article._id);
			} else {
				favouriteArticles.splice(favouriteArticles.indexOf(this.state.article._id), 1);
			}
			await axios.patch(`/api/users/${this.props.user._id}/favourite/articles`, {
				favouriteArticles: favouriteArticles
			}, { headers: { Authorization: `Bearer ${this.props.token}` } });
			this.props.updateFavouriteArticles(favouriteArticles);
		}
	}

	addComment = (comment) => {
		const article = this.state.article;
		article.comments.push(comment);
		this.setState({ article });
		console.log(this.state.article);
	}

	fetchProposedArticles = async () => {
		if (this.state.article) {
			const res = await axios.get(`/api/article/category/${this.state.article.category}/1`);
			this.setState({ articles: res.data.articles });
		}
	}

	render() {
		let proposedArticles = [];
		// Filter proposed articel to prevent displaying the currently read article in the proposed
		if (this.state.articles) {
			this.state.articles.forEach((article, i) => {
				if (article._id !== this.state.article._id)
					proposedArticles.push(<DetailedArticle onClick={this.loadArticle} key={article._id} article={article} />);
			});
		}

		return (
			<div className={styles.article}>
				{this.state.spinner ? <Spinner size={200} /> : null}

				{this.state.article ?
					<>
						<div className={styles.header}>
							<h3 className={styles.title}>{this.state.article.title}</h3>
							<img
								className={styles.favImage}
								src={this.props.user && this.props.user.favouriteArticles.indexOf(this.state.article._id) !== -1 ? require('../../images/star-full.png') : require('../../images/star.png')}
								alt='article favourite icon'
								onClick={this.addArticleToFavourite} />
						</div>
						<h2 className={styles.subtitle}>{this.state.article.subtitle}</h2>
						<img className={styles.coverPhoto} src={`/api/image/${this.state.article.imagePath}`} alt="cover" />
						<div className={styles.content}>
							<div className={styles.articleText}>
								<div dangerouslySetInnerHTML={{ __html: this.state.article.text }}></div>
								<AuthorDetails authorId={this.state.article.authorId} />
								<Comments addComment={this.addComment} comments={this.state.article.comments} articleId={this.props.match.params.articleId} />
							</div>
							<div className={styles.proposed}>
								<h3>Suggested from the same category: </h3>
								<div className={styles.proposedArticles}> {proposedArticles} </div>
							</div>
						</div>
					</>
					: null}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		token: state.user.token,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateFavouriteArticles: (array) => dispatch(actionCreators.updateFavouriteArticles(array))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);