export default {
	userName: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'USERNAME',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	password: {
		elementType: 'input',
		elementConfig: {
			type: 'password',
			placeholder: ''
		},
		value: '',
		label: 'PASSWORD',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
}