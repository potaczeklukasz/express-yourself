import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import Cookies from 'js-cookie';

import axios from 'axios';

import * as actionCreators from '../../store/actions/index';
import { updateObject, checkValidity } from '../../helpers/helpers';
import loginFields from './loginFields';
import registerFields from './registerFields';

import homeIcon from '../../images/home.png';

import Form from '../../components/Form/Form';
import Button from '../../components/UI/Button/Button';
import Modal from '../../components/UI/Modal/Modal';

import styles from './Auth.module.scss';

const modalRoot = document.getElementById("modal-root");

class Auth extends Component {
	state = {
		type: this.props.location.pathname, // '/login' or '/register'
		fields: this.props.location.pathname === '/login' ? loginFields : registerFields,
		showInputsErrors: false,
		errorMessage: '',
		modal: false,
		modalText: null
	}

	authUser = async (event) => {
		event.preventDefault();
		if (this.isFormValid()) {
			try {
				const res = await axios.post('/api/auth/login', {
					userName: this.state.fields.userName.value,
					password: this.state.fields.password.value
				});
				this.props.authUser(res.data.token, res.data.user);
				const hour = new Date(new Date().getTime() + 60 * 60 * 1000);
				Cookies.set('token', res.data.token, { expires: hour });
			} catch (err) {
				this.setState({ errorMessage: err.response.data.description });
			}
		} else {
			this.setState({ showInputsErrors: true });
		}
	}

	registerUser = async (event) => {
		event.preventDefault();
		if (this.isFormValid()) {
			if (this.state.fields.password.value !== this.state.fields.confirmPassword.value) return this.setState({ errorMessage: "Passwords do not match" });
			try {
				await axios.post('/api/users/register', {
					userName: this.state.fields.userName.value,
					email: this.state.fields.email.value,
					password: this.state.fields.password.value,
					firstName: this.state.fields.firstName.value,
					lastName: this.state.fields.lastName.value,
					sex: this.state.fields.sex.value || this.state.fields.sex.options[0].value,
					birthDate: new Date(this.state.fields.birthDate.value)
				});

				this.setState({ modal: true, modalText: "Your account has been successfully registered. You can login now." });

				this.switchForm();
			} catch (err) {
				this.setState({ errorMessage: err.response.data.description });
			}
		} else {
			this.setState({ showInputsErrors: true });
		}
	}

	inputChangedHandler = async (event, fieldName) => {
		const updatedFields = updateObject(this.state.fields, {
			[fieldName]: updateObject(this.state.fields[fieldName], {
				value: event.target.value,
				valid: checkValidity(event.target.value, this.state.fields[fieldName].validation),
				touched: true
			})
		});
		await this.setState({ fields: updatedFields });
	}

	isFormValid = () => {
		for (const key in this.state.fields) {
			if (!this.state.fields[key].valid.valid) return false;
		}
		return true;
	}

	switchForm = () => {
		if (this.state.type === '/login') {
			this.setState({ type: "/register", fields: registerFields, errorMessage: '', showInputsErrors: false });
		} else {
			this.setState({ type: "/login", fields: loginFields, errorMessage: '', showInputsErrors: false });
		}
	}

	hideModal = () => {
		this.setState({ modal: false, modalText: null });
	}

	render() {
		return (
			<div className={styles.auth}>
				{this.state.modal ? ReactDOM.createPortal(<Modal confirmHandler={this.hideModal} text={this.state.modalText} />, modalRoot) : null}

				{/* If user is already sign in, redirect to home page */}
				{this.props.isAuthenticated ? <Redirect to="/" /> : null}

				<div className={styles.main}>
					{this.state.type === '/register' ? <div className={styles.registerImage}></div> : null}
					<div className={styles.form}>
						{/* Back to home page button */}
						<Link to="/" className={styles.home}><img src={homeIcon} alt='home button' /></Link>

						<Form
							fields={this.state.fields}
							inputChangedHandler={this.inputChangedHandler}
							error={this.state.errorMessage}
							showInputsErrors={this.state.showInputsErrors}>
							<Button className={styles.button} clickHandler={this.state.type === "/login" ? this.authUser : this.registerUser} type="green">
								{this.state.type === '/login' ? "SIGN IN NOW" : "SIGN UP NOW"}
							</Button>
						</Form>

						{/* Switch from login to register and vice vers */}
						{this.state.type === '/login' ?
							<Link className={styles.link} to="/register" onClick={this.switchForm}>I do not have an account</Link> :
							<Link className={styles.link} to="/login" onClick={this.switchForm}>I am already a member</Link>}
					</div>
					{this.state.type === '/login' ? <div className={styles.loginImage}></div> : null}
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);