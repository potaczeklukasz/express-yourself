export default {
	userName: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'USERNAME',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	email: {
		elementType: 'input',
		elementConfig: {
			type: 'email',
			placeholder: ''
		},
		value: '',
		label: 'E-MAIL',
		validation: {
			required: true,
			isEmail: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	password: {
		elementType: 'input',
		elementConfig: {
			type: 'password',
			placeholder: ''
		},
		value: '',
		label: 'PASSWORD',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	confirmPassword: {
		elementType: 'input',
		elementConfig: {
			type: 'password',
			placeholder: ''
		},
		value: '',
		label: 'CONFIRM PASSWORD',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	firstName: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'FIRST NAME',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	lastName: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'LAST NAME',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	sex: {
		elementType: 'select',
		elementConfig: {
			type: 'select',
			placeholder: ''
		},
		value: '',
		options: [
			{ value: 'M', display: "MALE" },
			{ value: 'F', display: "FEMALE" },
			{ value: 'O', display: "OTHER" }
		],
		label: 'GENDER',
		validation: {
			required: true
		},
		valid: { valid: true },
		touched: false
	},
	birthDate: {
		elementType: 'input',
		elementConfig: {
			type: 'date',
			placeholder: ''
		},
		value: '',
		label: 'BIRTH DATE',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
}