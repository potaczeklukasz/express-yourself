import React, { Component } from 'react';
import axios from 'axios';
import BottomScrollListener from 'react-bottom-scroll-listener';

import DetailedArticle from '../../components/DetailedArticle/DetailedArticle';
import Spinner from '../../components/UI/Spinner/Spinner';

import lazyLoadingGif from '../../images/loading.gif';
import searchIcon from '../../images/search.png';

import styles from './CategoryArticles.module.scss';

export default class CategoryArticles extends Component {
	state = {
		page: 1,
		totalPages: null,
		articles: null,
		spinner: true,
		error: null,
		lazyLoading: false,
		loadingGif: true
	}

	componentDidMount = async () => {
		this.getArticles();
	}

	componentWillReceiveProps = async () => {
		await this.setState({ page: 1, totalPages: null, articles: null, loadingGif: true });
		this.getArticles();
	}

	checkIfBottom = () => {
		const d = document.documentElement;
		const offset = d.scrollTop + window.innerHeight;

		// if user is on the bottom of page, load more articles
		if (offset === document.body.scrollHeight && !this.state.lazyLoading) {
			this.setState({ lazyLoading: true });
			this.lazyLoad();
		}
	}

	getArticles = async () => {
		try {
			this.setState({ spinner: true });
			const res = await axios.get(`/api/article/category/${this.props.match.params.category}/${this.state.page}`);

			this.setState({ articles: res.data.articles, totalPages: res.data.totalPages, spinner: false, error: null, page: res.data.currentPage + 1 });

			if (res.data.totalPages >= res.data.currentPage + 1) this.checkIfBottom();
			else this.setState({ loadingGif: false });
		} catch (err) {
			this.setState({ error: err.response.data.description, spinner: false, loadingGif: false });
		}
	}

	lazyLoad = async () => {
		try {
			this.setState({ lazyLoading: true });
			const res = await axios.get(`/api/article/category/${this.props.match.params.category}/${this.state.page}`);

			// add loaded articles to already displayed articles
			const articles = this.state.articles;
			res.data.articles.forEach(article => {
				articles.push(article);
			});

			this.setState({ articles: articles, totalPages: res.data.totalPages, spinner: false, error: null, page: res.data.currentPage + 1, lazyLoading: false });

			// if this was last page of articles - remove loading gif
			if (res.data.totalPages < res.data.currentPage + 1) this.setState({ loadingGif: false });
		} catch (err) {
			this.setState({ error: err.response.data.description, spinner: false, loadingGif: false });
		}
	}

	render() {
		return (
			<>
				<BottomScrollListener onBottom={() => this.state.totalPages >= this.state.page ? this.checkIfBottom() : null} />
				{this.state.spinner ? <div className={styles.loading}><Spinner size={200} /></div> : null}
				<div className={styles.titleBox}>
					<p>Articles from category: {this.props.match.params.category}</p>
					<img src={searchIcon} alt="search" onClick={() => this.props.history.push('/search')} />
				</div>
				{this.state.error ? <p className={styles.errorMessage}>{this.state.error}</p> : null}
				{this.state.articles ?
					<div className={styles.categoryArticles}>
						{this.state.articles.map((article, i) => <DetailedArticle key={article._id} article={article} />)}
					</div> : null}
				{this.state.loadingGif && !this.state.spinner ? <img className={styles.loadingGif} src={lazyLoadingGif} alt='loading' /> : null}
			</>
		)
	}
}
