import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

import axios from 'axios';

// Editor imports
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import * as actionCreators from '../../../store/actions/index';
import formFields from './formFields';
import { updateObject, checkValidity } from '../../../helpers/helpers';

import Button from '../../../components/UI/Button/Button';
import Modal from '../../../components/UI/Modal/Modal';
import Form from '../../../components/Form/Form';


import styles from './EditArticle.module.scss';

const modalRoot = document.getElementById("modal-root");

class EditArticle extends Component {
	state = {
		type: this.props.location.pathname,
		fields: formFields,
		showInputsErrors: false,
		errorMessage: '',
		editorState: EditorState.createEmpty(),
		id: null,
		modal: false,
		modalText: null
	}

	static getDerivedStateFromProps = (props, state) => {
		if (props.id && props.id !== state.id) {
			const fields = { ...state.fields };
			fields.category.value = props.category;
			fields.title.value = props.title;
			fields.subtitle.value = props.subtitle;
			fields.imagePath.value = props.image;
			fields.imagePath.valid = { valid: true };
			fields.title.valid = { valid: true };
			fields.subtitle.valid = { valid: true };

			// update draft content with fetched text
			const blocksFromHtml = htmlToDraft(props.text);
			const { contentBlocks, entityMap } = blocksFromHtml;
			const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
			const editorState = EditorState.createWithContent(contentState);

			return { ...state, fields, id: props.id, editorState: editorState };
		} else {
			return { ...state }
		}
	}

	componentDidMount = () => {
		if (this.props.categories) {
			const categoriesArr = this.props.categories.map(category => ({ display: category.category, value: category.category }));
			const fields = { ...this.state.fields };
			fields.category.options = categoriesArr;
			this.setState({ fields });
		}
	}

	onEditorStateChange = (editorState) => {
		this.setState({ editorState });
	}

	submitHandler = async (event) => {
		event.preventDefault();
		if (this.isFormValid()) {
			if (this.state.id) {
				this.editArticle();
			} else {
				this.addNewArticle();
			}
		} else {
			this.setState({ showInputsErrors: true });
			if (!this.state.fields.imagePath.valid.valid) this.setState({ modal: true, modalText: "Tou need to upload photo" });
		}
	}

	addNewArticle = async () => {
		try {
			const res = await axios.post('/api/article', {
				authorId: this.props.user._id,
				imagePath: this.state.fields.imagePath.value,
				authorName: `${this.props.user.firstName} ${this.props.user.lastName}`,
				category: this.state.fields.category.value || this.state.fields.category.options[0].value,
				title: this.state.fields.title.value,
				subtitle: this.state.fields.subtitle.value,
				text: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
			}, { headers: { Authorization: `Bearer ${this.props.token}` } });

			this.setState({ modal: true, modalText: res.data.description });
			this.clear();
			this.props.refreshArticles();
		} catch (err) {
			this.setState({ errorMessage: err.response.data.description });
		}
	}

	editArticle = async () => {
		try {
			const res = await axios.patch(`/api/article/${this.state.id}`, {
				imagePath: this.state.fields.imagePath.value,
				category: this.state.fields.category.value || this.state.fields.category.options[0].value,
				title: this.state.fields.title.value,
				subtitle: this.state.fields.subtitle.value,
				text: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
			}, { headers: { Authorization: `Bearer ${this.props.token}` } });

			this.setState({ modal: true, modalText: res.data.description });
			this.clear();
			this.props.refreshArticles();
		} catch (err) {
			this.setState({ errorMessage: err.response.data.description });
		}
	}

	inputChangedHandler = async (event, fieldName) => {
		const updatedFields = updateObject(this.state.fields, {
			[fieldName]: updateObject(this.state.fields[fieldName], {
				value: event.target.value,
				valid: checkValidity(event.target.value, this.state.fields[fieldName].validation),
				touched: true
			})
		});
		await this.setState({ fields: updatedFields });
	}

	isFormValid = () => {
		for (const key in this.state.fields) {
			if (!this.state.fields[key].valid.valid) return false;
		}
		return true;
	}

	sendFile = async (image) => {
		if (image) {
			const formData = new FormData();
			formData.append('image', image, image.name);

			const res = await axios({
				url: '/api/image/upload',
				method: 'POST',
				headers: { 'Authorization': `Bearer ${this.props.token}`, 'Content-Type': 'multipart/form-data' },
				data: formData
			});
			if (!res.data.error) {
				const fields = this.state.fields;
				fields.imagePath.value = res.data.fileName;
				fields.imagePath.valid = { valid: true }
				this.setState({ fields });
			}
		}
	}

	clear = () => {
		this.props.clear();
		const fields = { ...this.state.fields };
		fields.category.value = "Tech";
		fields.title.value = "";
		fields.subtitle.value = "";
		fields.imagePath.value = "";
		fields.imagePath.valid = { valid: false, error: 'You need to upload image' };
		fields.title.valid = { valid: false, error: 'Field cannot be empty' };
		this.fileInput.value = "";
		this.setState({ id: null, fields: formFields, editorState: EditorState.createEmpty(), showInputsErrors: false })
	}

	hideModal = () => {
		this.setState({ modal: false, modalText: null });
	}

	deleteArticle = async () => {
		try {
			const res = await axios.delete(`/api/article/${this.state.id}`, { headers: { Authorization: `Bearer ${this.props.token}` } });
			if (res.data.error === false) {
				this.clear();
				this.props.refreshArticles();
			}
		} catch (err) {
			this.setState({ modal: true, modalText: err.response.data.description });
		}
	}

	// prevent crashing when paste images
	handlePastedText = (text) => text;

	render() {
		return (
			<div className={styles.editArticle}>
				{!this.props.isAuthenticated ? <Redirect to="/" /> : null}

				{this.state.modal ? ReactDOM.createPortal(<Modal confirmHandler={this.hideModal} text={this.state.modalText} />, modalRoot) : null}

				<label
					className={`${styles.label} ${this.state.fields.imagePath.valid.valid ? styles.blue : styles.red}`}
					htmlFor="upload-photo">{this.state.fields.imagePath.valid.valid ? "Photo selected" : "Select photo..."}</label>
				<input type="file" id="upload-photo" ref={ref => this.fileInput = ref} onChange={(e) => this.sendFile(e.target.files[0])} />

				<Form
					fields={this.state.fields}
					submitHandler={this.submitHandler}
					inputChangedHandler={this.inputChangedHandler}
					showInputsErrors={this.state.showInputsErrors}
					error={this.state.errorMessage}>
					<Editor
						onEditorStateChange={this.onEditorStateChange}
						editorState={this.state.editorState}
						editorClassName={styles.editor}
						toolbarClassName={styles.toolbar}
						handlePastedText={this.handlePastedText} />
					<div className={styles.buttons}>
						<Button className={styles.button} clickHandler={this.clear} type="blue">CLEAR</Button>
						{this.state.id ? <Button className={styles.button} clickHandler={this.deleteArticle} type="red">DELETE</Button> : null}
						<Button className={styles.button} clickHandler={this.submitHandler} type="green">{this.state.id ? "EDIT ARTICLE" : "ADD ARTICLE"}</Button>
					</div>
				</Form>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		token: state.user.token,
		user: state.user.user,
		categories: state.category.categories
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditArticle));