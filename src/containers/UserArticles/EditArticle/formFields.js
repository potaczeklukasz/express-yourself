export default {
	category: {
		elementType: 'select',
		elementConfig: {
			type: 'select',
			placeholder: ''
		},
		value: '',
		options: [],
		label: 'CATEGORY',
		validation: {
			required: true
		},
		valid: { valid: true },
		touched: false
	},
	title: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'TITLE',
		validation: {
			required: true,
			maxLength: 100
		},
		valid: { valid: false, error: 'Field cannot be empty' },
		touched: false
	},
	subtitle: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			placeholder: ''
		},
		value: '',
		label: 'SUBTITLE',
		validation: {
			maxLength: 120
		},
		valid: { valid: true },
		touched: false
	},
	imagePath: {
		elementType: 'input',
		elementConfig: {
			type: 'text',
			hidden: true
		},
		value: '',
		label: '',
		validation: {
			required: true
		},
		valid: { valid: false, error: 'You need to upload image' },
		touched: false
	},
}