import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { Pie, Bar, Line } from 'react-chartjs-2';

import styles from './ArticleStatistics.module.scss';

import Button from '../../../components/UI/Button/Button';

import { setViewsData } from '../../../helpers/helpers';

class ArticleStatistics extends Component {
	state = {
		statistics: null,
		articleId: this.props.articleId,
		ageData: {
			labels: ["<18", "18-20", "21-26", "27-35", "36-45", "46-60", "60>"],
			datasets: [
				{
					label: "Amount",
					data: [],
					backgroundColor: [
						'rgb(123, 211, 0)',
						'rgb(210, 210, 0)',
						'rgb(255, 211, 15)',
						'rgb(255, 179, 15)',
						'rgb(255, 135, 15)',
						'rgb(255, 103, 15)',
						'rgb(255, 63, 15)'
					]
				}
			]
		},
		genderData: {
			labels: ["Man", "Woman", "Other"],
			datasets: [
				{
					label: "Gender",
					data: [],
					backgroundColor: [
						'rgb(84, 146, 247)',
						'rgb(253, 160, 255)',
						'rgb(124, 124, 124)'
					]
				}
			]
		},
		viewsData: {
			labels: [],
			datasets: [
				{
					label: "Amount",
					data: [],
					backgroundColor: [
						'rgb(123, 211, 0)'
					]
				}
			]
		}
	}

	componentDidMount = async () => {
		if (this.props.isAuthenticated) {
			const statistics = await axios.get(`/api/article/${this.props.articleId}/statistics`);
			this.setState({ statistics: statistics.data.statistics });
			this.countGender();
			this.countAge();
			this.countViwes();
		}
	}

	componentWillUpdate = async (props, state) => {
		if (this.props.isAuthenticated) {
			if (this.state.articleId !== props.articleId) {
				const statistics = await axios.get(`/api/article/${props.articleId}/statistics`);
				this.setState({ statistics: statistics.data.statistics, articleId: props.articleId });
				this.countGender();
				this.countAge();
				this.countViwes();
			}
		}
	}

	countViwes = () => {
		if (this.state.statistics) {
			const viewsData = setViewsData(this.state.statistics);
			const viewsDataState = this.state.viewsData;

			viewsDataState.labels = viewsData.labels;
			viewsDataState.datasets[0].data = viewsData.values;

			this.setState({ viewsData: viewsDataState });
		}
	}

	countGender = () => {
		let M = 0;
		let F = 0;
		let O = 0;

		if (this.state.statistics) {
			this.state.statistics.forEach(data => {
				if (data.userGender === "M") M++;
				if (data.userGender === "F") F++;
				if (data.userGender === "O") O++;
			})
			const genderData = this.state.genderData;
			genderData.datasets[0].data = [];
			genderData.datasets[0].data.push(M, F, O);
			this.setState({ genderData });
		}
	}

	countAge = () => {
		const ages = [0, 0, 0, 0, 0, 0, 0];

		if (this.state.statistics) {
			this.state.statistics.forEach(data => {
				if (data.userAge === "<18") {
					ages[0]++;
				} else if (data.userAge === "18-20") {
					ages[1]++;
				} else if (data.userAge === "21-26") {
					ages[2]++;
				} else if (data.userAge === "27-35") {
					ages[3]++;
				} else if (data.userAge === "36-45") {
					ages[4]++;
				} else if (data.userAge === "46-60") {
					ages[5]++;
				} else {
					ages[6]++;
				}
			})
			const ageData = this.state.ageData;
			ageData.datasets[0].data = ages;
			this.setState({ ageData });
		}
	}

	render() {
		return (
			<div className={styles.articleStatistics}>
				<Button className={styles.button} clickHandler={() => this.props.history.push('/profile/articles/new')} type="blue">ADD NEW ARTICLE</Button>
				<div className={styles.smallCharts}>
					<div className={styles.smallChart}>
						<p className={styles.title}>Users gender</p>
						<Pie data={this.state.genderData} height={300} options={{ maintainAspectRatio: false, responsive: true }} />
					</div>
					<div className={styles.smallChart}>
						<p className={styles.title}>Users age</p>
						<Bar data={this.state.ageData} height={300} options={{ legend: { display: false }, maintainAspectRatio: false, responsive: true }} />
					</div>
				</div>
				<div className={styles.bigChart}>
					<p className={styles.title}>Number of visits</p>
					<Line data={this.state.viewsData} height={300} options={{ legend: { display: false }, maintainAspectRatio: false }} />
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user,
		token: state.user.token
	};
};

export default connect(mapStateToProps)(withRouter(ArticleStatistics));