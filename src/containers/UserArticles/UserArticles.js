import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';

// import * as actionCreators from '../../store/actions/index';

import styles from './UserArticles.module.scss';

import UserArticlesList from '../../components/UserArticlesList/UserArticlesList';
import EditArticle from './EditArticle/EditArticle';
import ArticleStatistics from './ArticleStatistics/ArticleStatistics';

class UserArticles extends Component {
	state = {
		type: this.props.location.pathname,
		articles: null,
		id: null,
		category: null,
		title: null,
		subtitle: null,
		image: null,
		text: null,
		statisticsArticleId: null
	}

	componentDidMount = async () => {
		this.getArticles();
	}

	getArticles = async () => {
		if (this.props.isAuthenticated) {
			const res = await axios.post('/api/article/user', {}, { headers: { Authorization: `Bearer ${this.props.token}` } });
			this.setState({ articles: res.data.articles });
		}
	}

	editArticle = async (id) => {
		const res = await axios.get(`/api/article/${id}`);
		this.setState({
			id: res.data.article._id,
			category: res.data.article.category,
			title: res.data.article.title,
			subtitle: res.data.article.subtitle,
			image: res.data.article.imagePath,
			text: res.data.article.text,
			type: "/profile/articles/new",
			statisticsArticleId: this.props.match.params.articleId
		});
		this.props.history.push("/profile/articles/new");
	}

	refreshArticles = async () => {
		this.getArticles();
	}

	static getDerivedStateFromProps = (props, state) => {
		return {
			...state,
			type: props.location.pathname
		}
	}

	updateArticleId = (articleId) => {
		this.setState({statisticsArticleId: articleId})
	}

	clear = () => {
		this.setState({
			id: null,
			category: null,
			title: null,
			subtitle: null,
			image: null,
			text: null,
			statisticsArticleId: null
		});
	}

	render() {
		return (
			<div className={styles.userArticles}>
				{!this.props.isAuthenticated ? <Redirect to="/" /> : null}
				<div className={styles.userArticlesList}>
					{this.state.articles ? <UserArticlesList articles={this.state.articles} editArticle={this.editArticle} updateId={this.updateArticleId}/> : null}
				</div>
				<div className={styles.editArticle}>
					{this.state.type === "/profile/articles/new" ?
						<EditArticle
							refreshArticles={this.refreshArticles}
							category={this.state.category}
							title={this.state.title}
							subtitle={this.state.subtitle}
							image={this.state.image}
							text={this.state.text}
							id={this.state.id}
							clear={this.clear} /> : <ArticleStatistics articleId={this.state.statisticsArticleId} />}
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user,
		token: state.user.token
	};
};

export default connect(mapStateToProps)(UserArticles);