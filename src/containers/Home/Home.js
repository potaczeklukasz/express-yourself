import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import * as actionCreators from '../../store/actions/index';

import ArticleContainer from '../../components/ArticlesContainer/ArticlesContainer';
import Spinner from '../../components/UI/Spinner/Spinner';

import styles from './Home.module.scss';

class Home extends Component {
	state = {
		newestArticles: null,
		popularArticles: null,
		error: null,
		spinner: true
	}

	componentDidMount = async () => {
		try {
			const res = await axios.get('/api/article');
			this.setState({ newestArticles: res.data.newestArticles, popularArticles: res.data.popularArticles, spinner: false, error: null });
		} catch (err) {
			this.setState({ error: err.response.data.description, spinner: false });
		}
	}

	render() {
		return (
			<div className={styles.home}>
				{this.state.spinner ? <Spinner size={200} /> : null}
				{this.state.error ? <p className={styles.errorMessage}>{this.state.error}. Try again later.</p> : null}
				{this.state.newestArticles ?
					<ArticleContainer
						title="Latest."
						subtitle="The latest articles from all categories."
						articles={this.state.newestArticles.articles} /> : null}
				{this.state.popularArticles ?
					<ArticleContainer
						title="Popular."
						subtitle="The most popular articles from all categories and all the time."
						articles={this.state.popularArticles.articles} /> : null}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token) => dispatch(actionCreators.authUser(token))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);